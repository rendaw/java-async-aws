package com.zarbosoft.asyncaws;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.BaseEncoding;
import com.google.common.net.InetAddresses;
import com.zarbosoft.coroutines.SuspendableSupplier;
import com.zarbosoft.coroutines.WRCriticalSection;
import com.zarbosoft.coroutinescore.SuspendExecution;
import com.zarbosoft.gettus.Cogettus;
import com.zarbosoft.rendaw.common.ChainComparator;
import com.zarbosoft.rendaw.common.Common;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unbescape.html.HtmlEscape;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xnio.XnioWorker;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.zarbosoft.rendaw.common.Common.substr;
import static com.zarbosoft.rendaw.common.Common.uncheck;
import static io.undertow.util.Headers.HOST;
import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.MINUTES;

/**
 * Class that aggregates static methods for interacting with AWS.  See `send`.
 */
public class AWS {
	private final static Logger logger = LoggerFactory.getLogger("aws");
	static XPath xpath = null;
	static DocumentBuilder xml = null;
	public static Transformer xmlWriter = null;

	/**
	 * Initialize reusable xml structures.  Used by dumpXml
	 *
	 * @return
	 */
	public static DocumentBuilder xml() {
		if (xml == null) {
			xml = uncheck(() -> DocumentBuilderFactory.newInstance().newDocumentBuilder());
			xmlWriter = uncheck(() -> TransformerFactory.newInstance().newTransformer());
		}
		return xml;
	}

	// From https://github.com/aws/aws-sdk-java/blob/7844c64cf248aed889811bf2e871ad6b276a89ca/aws-java-sdk-core/src/main/java/com/amazonaws/util/SdkHttpUtils.java
	// Apache license 2.0
	private static final Pattern AWS_ENCODED_CHARACTERS_PATTERN;

	static {
		final StringBuilder pattern = new StringBuilder();

		pattern
				.append(Pattern.quote("+"))
				.append("|")
				.append(Pattern.quote("*"))
				.append("|")
				.append(Pattern.quote("%7E"))
				.append("|")
				.append(Pattern.quote("%2F"));

		AWS_ENCODED_CHARACTERS_PATTERN = Pattern.compile(pattern.toString());
	}

	/**
	 * AWS compatible url encoding
	 *
	 * @param value
	 * @param path
	 * @return
	 */
	public static String urlEncode(final String value, final boolean path) {
		// From https://github.com/aws/aws-sdk-java/blob/7844c64cf248aed889811bf2e871ad6b276a89ca/aws-java-sdk-core/src/main/java/com/amazonaws/util/SdkHttpUtils.java
		// Apache license 2.0
		// Inlined variables, used uncheck
		if (value == null) {
			return "";
		}

		final String encoded = uncheck(() -> URLEncoder.encode(value, "utf-8"));

		final Matcher matcher = AWS_ENCODED_CHARACTERS_PATTERN.matcher(encoded);
		final StringBuffer buffer = new StringBuffer(encoded.length());

		while (matcher.find()) {
			String replacement = matcher.group(0);

			if ("+".equals(replacement)) {
				replacement = "%20";
			} else if ("*".equals(replacement)) {
				replacement = "%2A";
			} else if ("%7E".equals(replacement)) {
				replacement = "~";
			} else if (path && "%2F".equals(replacement)) {
				replacement = "/";
			}

			matcher.appendReplacement(buffer, replacement);
		}

		matcher.appendTail(buffer);
		return buffer.toString();
	}

	/**
	 * AWS compatible sha256 hash chain (result hashed with next value)
	 *
	 * @param values
	 * @return
	 */
	public static byte[] hmacSHA256Chain(final String... values) {
		return uncheck(() -> {
			byte out[] = null;
			for (final String value : values) {
				final byte[] temp = value.getBytes(StandardCharsets.UTF_8);
				if (out == null) {
					out = temp;
				} else {
					final String algorithm = "HmacSHA256";
					final Mac mac = Mac.getInstance(algorithm);
					mac.init(new SecretKeySpec(out, algorithm));
					out = mac.doFinal(temp);
				}
			}
			return out;
		});
	}

	/**
	 * AWS compatible sha256 hash
	 *
	 * @param data
	 * @return
	 */
	public static byte[] hashSHA256(final byte[] data) {
		return uncheck(() -> MessageDigest.getInstance("SHA-256")).digest(data);
	}

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("uuuuMMdd'T'HHmmss'Z'");

	/**
	 * This is the primary useful method.  Sends a request to AWS's API.
	 *
	 * @param worker
	 * @param region
	 * @param accessKey
	 * @param accessSecret
	 * @param method
	 * @param service
	 * @param host
	 * @param path
	 * @param headers
	 * @param query
	 * @param body
	 * @return the unprocessed response
	 * @throws SuspendExecution
	 */
	public static Cogettus.Headers send(
			final XnioWorker worker,
			final String region,
			final String accessKey,
			final String accessSecret,
			final HttpString method,
			final String service,
			final String host,
			final String path,
			final Map<HttpString, String> headers,
			final Map<String, String> query,
			final byte[] body
	) throws SuspendExecution {
		final BaseEncoding b16 = BaseEncoding.base16();
		final String query0 = Optional
				.ofNullable(query)
				.orElse(ImmutableMap.of())
				.entrySet()
				.stream()
				.sorted(new ChainComparator<Map.Entry<String, String>>().lesserFirst(a -> a.getKey()).build())
				.map(e -> String.format("%s=%s",
						uncheck(() -> urlEncode(e.getKey(), false)),
						uncheck(() -> urlEncode(e.getValue(), false))
				))
				.collect(Collectors.joining("&"));
		logger.debug(String.format("Canonical Query:\n--------\n%s\n--------\n", query0));
		//logger.debug(String.format("Body:\n--------\n%s\n--------\n", new String(body, StandardCharsets.UTF_8)));
		final String host0 = String.format("%s.amazonaws.com", host);
		final Map<HttpString, String> headers0 = Optional.ofNullable(headers).map(HashMap::new).orElseGet(HashMap::new);
		headers0.putIfAbsent(HOST, host0);
		final ZonedDateTime utcNow = ZonedDateTime.now().withZoneSameInstant(UTC);
		final LocalDate utcNowDate = utcNow.toLocalDate();
		final String dateTimeString = utcNow.format(dateTimeFormatter);
		headers0.putIfAbsent(HttpString.tryFromString("x-amz-date"), dateTimeString);
		final String bodyHash = b16.encode(hashSHA256(body == null ? new byte[] {} : body)).toLowerCase();
		headers0.putIfAbsent(HttpString.tryFromString("x-amz-content-sha256"), bodyHash);
		if (body != null) {
			final byte[] hash = uncheck(() -> MessageDigest.getInstance("MD5")).digest(body);
			headers0.putIfAbsent(Headers.CONTENT_MD5, BaseEncoding.base64().encode(hash));
		}
		// Documented: http://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html
		final String signedHeaders = headers0
				.entrySet()
				.stream()
				.sorted(new ChainComparator<Map.Entry<HttpString, String>>().lesserFirst(e -> e.getKey()).build())
				.map(e -> e.getKey().toString().toLowerCase())
				.collect(Collectors.joining(";"));
		final String dateString = utcNowDate.format(DateTimeFormatter.BASIC_ISO_DATE);
		final String awsAlgorithm = "AWS4-HMAC-SHA256";
		final String version = "aws4_request";
		final String credentialScope = Stream.of(dateString, region, service, version).collect(Collectors.joining("/"));
		final String canonicalRequest = Stream.of(method.toString(),
				path,
				query0,
				headers0
						.entrySet()
						.stream()
						.sorted(new ChainComparator<Map.Entry<HttpString, String>>()
								.lesserFirst(e -> e.getKey())
								.build())
						.map(e -> String.format("%s:%s\n", e.getKey().toString().toLowerCase(), e.getValue()))
						.collect(Collectors.joining()),
				signedHeaders,
				bodyHash
		).collect(Collectors.joining("\n"));
		logger.debug(String.format("CanonicalRequest:\n--------\n%s\n--------\n", canonicalRequest));
		final String stringToSign = Stream.of(awsAlgorithm,
				dateTimeString,
				credentialScope,
				BaseEncoding
						.base16()
						.encode(hashSHA256(canonicalRequest.getBytes(StandardCharsets.UTF_8)))
						.toLowerCase()
		).collect(Collectors.joining("\n"));
		logger.debug(String.format("StringToSign:\n--------\n%s\n--------\n", stringToSign));
		final String signature = b16
				.encode(hmacSHA256Chain("AWS4" + accessSecret, dateString, region, service, version, stringToSign))
				.toLowerCase();
		logger.debug(String.format("Signature:\n--------\n%s\n--------\n", signature));
		final String authHeader = String.format("%s Credential=%s/%s, SignedHeaders=%s, Signature=%s",
				awsAlgorithm,
				accessKey,
				credentialScope,
				signedHeaders,
				signature
		);
		logger.debug(String.format("AUTH HEADER:\n--------\n%s\n--------\n", authHeader));
		headers0.put(Headers.AUTHORIZATION, authHeader);
		final URI uri = uncheck(() -> new URI("https", host0, path, query0.isEmpty() ? null : query0, null));
		logger.debug(String.format("URI:\n--------\n%s\n--------\n", uri));
		return new Cogettus(worker, uri).method(method).headers(headers0).body(body).send();
	}

	private static class InstanceCredentials {
		Instant expiration;
		String access;
		String secret;
		String session;
	}

	private static Map<String, InstanceCredentials> instanceCredentials = new HashMap<>();
	private static WRCriticalSection credentialMutex = new WRCriticalSection();

	private static InstanceCredentials getInstanceCredentials(XnioWorker worker, String role) throws SuspendExecution {
		SuspendableSupplier<InstanceCredentials> read = () -> {
			return instanceCredentials.get(role);
		};
		InstanceCredentials credentials = credentialMutex.read(worker, read);
		if (credentials != null && Instant.now().plus(5, MINUTES).isBefore(credentials.expiration))
			return credentials;
		credentialMutex.tryUniqueWrite(worker, () -> {
			JsonNode resp;
			if (role == null) {
				resp = new Cogettus(worker,
						Cogettus.formatURI("http://169.254.170.2%s",
								System.getenv("AWS_CONTAINER_CREDENTIALS_RELATIVE_URI")
						)
				)
						.host()
						.send()
						.body()
						.check()
						.json();
			} else {
				resp = new Cogettus(worker,
						Cogettus.formatURI("http://169.254.169.254/latest/meta-data/iam/security-credentials/%s", role)
				).send().body().check().json();
			}
			InstanceCredentials result = new InstanceCredentials();
			result.expiration =
					ZonedDateTime.parse(resp.get("Expiration").asText(), DateTimeFormatter.ISO_DATE_TIME).toInstant();
			result.access = resp.get("AccessKeyId").asText();
			result.secret = resp.get("SecretAccessKey").asText();
			result.session = resp.get("Token").asText();
			instanceCredentials.put(role, result);
		});
		return credentialMutex.read(worker, read);
	}

	/**
	 * Send an AWS API request using default env/instance credentials.
	 *
	 * @param worker
	 * @param region
	 * @param roleName the role in the instance metadata that contains the required permissions
	 * @param method
	 * @param service
	 * @param host
	 * @param path
	 * @param headers
	 * @param query
	 * @param body
	 * @return
	 * @throws SuspendExecution
	 */
	public static Cogettus.Headers send(
			final XnioWorker worker,
			final String region,
			final String roleName,
			final HttpString method,
			final String service,
			final String host,
			final String path,
			final Map<HttpString, String> headers,
			final Map<String, String> query,
			final byte[] body
	) throws SuspendExecution {
		InstanceCredentials credentials = getInstanceCredentials(worker, roleName);
		Map<HttpString, String> headers1 = Optional.ofNullable(headers).map(HashMap::new).orElseGet(HashMap::new);
		headers1.put(HttpString.tryFromString("X-Amz-Security-Token"), credentials.session);
		return send(worker,
				region,
				credentials.access,
				credentials.secret,
				method,
				service,
				host,
				path,
				headers1,
				query,
				body
		);
	}

	private static String defaultAZ;
	private static WRCriticalSection defaultAZMutex = new WRCriticalSection();

	public static String getDefaultAZ(XnioWorker worker) throws SuspendExecution {
		{
			String out = System.getenv("AWS_DEFAULT_REGION");
			if (out != null)
				return out;
		}
		String out = defaultAZMutex.read(worker, () -> defaultAZ);
		if (out != null)
			return out;
		defaultAZMutex.tryUniqueWrite(worker, () -> {
			defaultAZ = new Cogettus(worker,
					Cogettus.formatURI("http://169.254.169.254/latest/meta-data/placement/availability-zone")
			)
					.send()
					.body()
					.check()
					.text();
		});
		return defaultAZMutex.read(worker, () -> defaultAZ);
	}

	public static String getDefaultRegion(XnioWorker worker) throws SuspendExecution {
		return substr(getDefaultAZ(worker), 0, -1);
	}

	private static String defaultRole;
	private static WRCriticalSection defaultRoleMutex = new WRCriticalSection();

	private static String getDefaultRole(XnioWorker worker) throws SuspendExecution {
		String out = defaultRoleMutex.read(worker, () -> defaultRole);
		if (out != null)
			return out;
		defaultRoleMutex.tryUniqueWrite(worker, () -> {
			String[] roles = new Cogettus(worker,
					Cogettus.formatURI("http://169.254.169.254/latest/meta-data/iam/security-credentials/")
			)
					.send()
					.body()
					.check()
					.text()
					.split("\n");
			String role = Arrays.stream(roles).filter(line -> !line.isEmpty()).findFirst().get();
			logger.debug(String.format("Found instance iam roles %s; using %s",
					Arrays.stream(roles).collect(Collectors.joining(", ")),
					role
			));
			defaultRole = role;
		});
		return defaultRoleMutex.read(worker, () -> defaultRole);
	}

	/**
	 * Use default instance metadata credentials (first listed credentials).
	 *
	 * @param worker
	 * @param region
	 * @param method
	 * @param service
	 * @param host
	 * @param path
	 * @param headers
	 * @param query
	 * @param body
	 * @return
	 * @throws SuspendExecution
	 */
	public static Cogettus.Headers send(
			final XnioWorker worker,
			final String region,
			final HttpString method,
			final String service,
			final String host,
			final String path,
			final Map<HttpString, String> headers,
			final Map<String, String> query,
			final byte[] body
	) throws SuspendExecution {
		String access = System.getenv("AWS_ACCESS_KEY_ID");
		String secret = System.getenv("AWS_SECRET_ACCESS_KEY");
		String taskCredentials = System.getenv("AWS_CONTAINER_CREDENTIALS_RELATIVE_URI");
		if (access != null && !access.isEmpty() && secret != null && !secret.isEmpty())
			return send(worker, region, access, secret, method, service, host, path, headers, query, body);
		else
			return send(worker,
					region,
					taskCredentials == null ? getDefaultRole(worker) : null,
					method,
					service,
					host,
					path,
					headers,
					query,
					body
			);
	}

	/**
	 * Iterate all nodes under an xpath
	 *
	 * @param expression
	 * @param node
	 * @return
	 */
	public static Iterable<Node> xpathIter(final String expression, final Object node) {
		final NodeList list = uncheck(() -> ((NodeList) xpath.evaluate(expression, node, XPathConstants.NODESET)));
		return new Iterable<Node>() {
			@Override
			public Iterator<Node> iterator() {
				return new Iterator<Node>() {
					int index = 0;

					@Override
					public boolean hasNext() {
						return index < list.getLength();
					}

					@Override
					public Node next() {
						return list.item(index++);
					}
				};
			}
		};
	}

	/**
	 * Extracts all text under an xpath
	 *
	 * @param expression
	 * @param node
	 * @return combined string of all text nodes
	 */
	public static String xpathString(final String expression, final Object node) {
		return HtmlEscape.unescapeHtml(Common
				.stream(xpathIter(expression, node))
				.map(n -> n.getTextContent())
				.collect(Collectors.joining("")));
	}

	/**
	 * Helper to convert an inputstream into an Xml document
	 *
	 * @param stream
	 * @return
	 */
	public static Document parseXml(final InputStream stream) {
		if (xpath == null)
			xpath = XPathFactory.newInstance().newXPath();
		return uncheck(() -> xml().parse(stream));
	}

	/**
	 * Convert an Xml document into bytes
	 *
	 * @param doc
	 * @return
	 */
	public static byte[] dumpXml(final Document doc) {
		final ByteArrayOutputStream target = new ByteArrayOutputStream();
		uncheck(() -> xmlWriter.transform(new DOMSource(doc), new StreamResult(target)));
		return target.toByteArray();
	}

	/**
	 * @param worker
	 * @return The private ip address of the ec2 instance the code is run on
	 * @throws SuspendExecution
	 */
	public static InetAddress localIp(final XnioWorker worker) throws SuspendExecution {
		return InetAddresses.forString(new Cogettus(worker,
				Cogettus.formatURI("http://169.254.169.254/latest/meta-data/local-ipv4")
		)
				.send()
				.body()
				.check()
				.text());
	}
}
